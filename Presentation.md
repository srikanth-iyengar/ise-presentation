---
marp: true
math: mathjax
theme: rose-pine
# theme: rose-pine-dawn
# theme: rose-pine-moon
---

<style lang=css>
/*
Rosé Pine theme create by RAINBOWFLESH
> www.rosepinetheme.com

palette in :root
*/

@import "default";
@import "schema";
@import "structure";

:root {
  --base: #232136;
    --surface: #2a273f;
    --overlay: #393552;
    --muted: #6e6a86;
    --subtle: #908caa;
    --text: #e0def4;
    --love: #eb6f92;
    --gold: #f6c177;
    --rose: #ea9a97;
    --pine: #3e8fb0;
    --foam: #9ccfd8;
    --iris: #c4a7e7;
    --highlight-low: #2a283e;
    --highlight-muted: #44415a;
    --highlight-high: #56526e;

  font-family: Pier Sans, ui-sans-serif, system-ui, -apple-system,
    BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans,
    sans-serif, "Apple Color Emoji", "Segoe UI Emoji", Segoe UI Symbol,
    "Noto Color Emoji";
  font-weight: initial;

  background-color: var(--base);
}
/*Common style*/
h1 {
  color: var(--rose);
  padding-bottom: 2mm;
  margin-bottom: 12mm;
}
h2 {
  color: var(--rose);
}
h3 {
  color: var(--rose);
}
h4 {
  color: var(--rose);
}
h5 {
  color: var(--rose);
}
h6 {
  color: var(--rose);
}
a {
  color: var(--iris);
}
p {
  font-size: 20pt;
  font-weight: 600;
  color: var(--text);
}
code {
  color: var(--text);
  background-color: var(--highlight-muted);
}
text {
  color: var(--text);
}
ul {
  color: var(--subtle);
}
li {
  color: var(--subtle);
}
img {
  background-color: var(--highlight-low);
}
strong {
  color: var(--text);
  font-weight: inherit;
  font-weight: 800;
}
mjx-container {
  color: var(--text);
}
marp-pre {
  background-color: var(--overlay);
  border-color: var(--highlight-high);
}

/*Code blok*/
.hljs-comment {
  color: var(--muted);
}
.hljs-attr {
  color: var(--foam);
}
.hljs-punctuation {
  color: var(--subtle);
}
.hljs-string {
  color: var(--gold);
}
.hljs-title {
  color: var(--foam);
}
.hljs-keyword {
  color: var(--pine);
}
.hljs-variable {
  color: var(--text);
}
.hljs-literal {
  color: var(--rose);
}
.hljs-type {
  color: var(--love);
}
.hljs-number {
  color: var(--gold);
}
.hljs-built_in {
  color: var(--love);
}
.hljs-params {
  color: var(--iris);
}
.hljs-symbol {
  color: var(--foam);
}
.hljs-meta {
  color: var(--subtle);
}

</style>
![bg right](./images/bg.png)
### Internship Evaluation
###### 🎓 Journey at Vyas Edification
###### 👤 Srikanth Iyengar
###### 🏢 Company: Vyas Edification
###### 👨‍💻 Job Title: SDE Intern
###### 📅 Date: 29/06/2024

---

## 🏆 Milestones covered from Jan to Mar:
- 🛠 Improving latex tool for question digitization
- 🐞 Bug fix in existing faculty dashboard tool
- 📱 Development of next-gen learning app for students
  - 🔍 Research on Development of a custom framework like Capacitor JS ( in flutter )
  - ⚙️ Choosing react native due to performance issue with earlier approach

---

## 🏆 Milestones covered from Apr to Jun:

- 🛠️ TRPC integration with serverless environment for typesafe and robust API
- 📽️ Automated video pipeline which generates sprites/caption and converts video for different resolution
- 📈 Integration of google ads in Bharatbrainz app for non subscription users
- 🚀 Deployment of ec2 based internal applications

---

## 🖥️ TRPC Integration for bharatbrainz app:
- 🔄 Changing the API gateway in CDK to cater Rest Api for trpc based lambdas
- 📡 Integration with Event Bus and API gateway for event based processing and web sockets
- 🧩 Developing a lambda template for future creation of trpc based lambda functions

---

## 🎥 Video pipeline for processing videos automatically
- 🏗️ Video pipeline integration:
  - 🛎️ Configure lambda to trigger AWS transcribe job to convert a single mp4 video to different resolution and make it streamable
  - 🎞️ Using ffmpeg to generate video sprites for the entire video duration
- ⚙️ Used ansible and scripting and enforce deployment consistency for deploying ec2 based applications

---

## 📺Google admobs sdk integration
- 📝 Figuring out scope for ads in app
- 🎁 Integrate reward based ads in places where user is rewarded or giving oppurtunity for user to earn some free rewards
-  ▶️ Using google IMA SDK to play ads inside the video player itself like youtube

---

## 🛠️ Tasks worked on actual app development
![bg right:40%](./images/eyes.gif)
- 🌐 Developing onboarding APIs and integrating them
- 📚 Learn module integration and video player engineering on client side
- 🕵️ Global network inspector on client side to track api failures, api latency and app crashes
- 🎯 Quest system engineering and client side integration


---
![bg right:40%](./images/luca-bravo-XJXWbfSo2f0-unsplash.jpg)
## 🛠 Technical Skills Learned:
- 🖥️ **Frontend:** React, React Query, Tailwind CSS, Deployment with CloudFront, React Native
- 💼 **Backend:** TRPC, Dynamodb, AWS Lambda, MongoDB
- 🛠 **Infrastructure:** CloudFormation, API Gateway, Lambda, CloudWatch, GitHub Actions, Shell Scripting

---

## 👥 Team
- 🤝 Collaborated with a diverse team of 10 professionals, including developers, designers, and stakeholders.
- 🗓️ Participated in daily stand-ups, sprint planning, and retrospective meetings, gaining insights into agile development practices.
- 👨‍💻 Enhanced teamwork skills through pair programming sessions and peer code reviews.
- 🚀 Developed leadership skills by taking ownership of tasks and driving project milestones forward.

--- 
## 🙏 Thank You
🙏 Thank you for your time and attention!
🌟 Open to any questions or discussions.

